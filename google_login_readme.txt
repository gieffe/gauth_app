pip install --upgrade google-api-python-client
pip install --upgrade google-auth google-auth-oauthlib google-auth-httplib2
pip install --upgrade requests

see: https://developers.google.com/identity/sign-in/web/

https://console.developers.google.com/

add to site_config.py:

 'GOOGLE': {u'auth_provider_x509_cert_url': u'https://www.googleapis.com/oauth2/v1/certs',
			u'auth_uri': u'https://accounts.google.com/o/oauth2/auth',
			u'client_id': u'webapp client id',
			u'client_secret': u'webapp client secret',
			u'javascript_origins': [list of javascript https origin urls],
			u'project_id': u'webapp project id',
			u'token_uri': u'https://oauth2.googleapis.com/token'}


