
import wsgian.utils

from wsgian import template, gauth


def main(pard):
	pard.setdefault('action', '')
	
	if pard['action'] in ('', 'start'):
		if pard['user_data']:
			pard['main_body'] = ''
			return render_main(pard)
		else:
			return render_home(pard)
	
	elif  pard['action'] == 'pippo':
		pard['html'] = wsgian.utils.dump(pard['user_data'])
		
	else:
		pard['html'] = pard['dump_user'] = wsgian.utils.dump(pard['action'])
	
	return pard


def render_home(pard):
	pard.setdefault('TITLE', '')
	pard.setdefault('PROJECT', 'gauth_app')
	pard.setdefault('PROJECT_TITLE', 'GAuth')
	pard.setdefault('PROJECT_SUBTITLE', 'Gian\'s basic authentication project')
		
	pard['main_body'] = '''
		<br />
		<a class="btn btn-success btn-lg" href="%(SIGNUP_URL)s" role="button">Sign-up it's free</a>
		<br />
		<br />
		Already have an account?
		<a href="%(LOGIN_URL)s">Log in</a>
		''' % pard
	
	pard['html'] = template.render('basic.html', pard, force_reload=True)
	return pard


def render_main(pard):
	pard.setdefault('TITLE', '')
	pard.setdefault('PROJECT', 'gauth_app')
	pard.setdefault('PROJECT_TITLE', 'GAuth')
	pard.setdefault('PROJECT_SUBTITLE', 'Gian\'s basic authentication project')
	#pard['dump_user'] = wsgian.utils.dump(pard['user_data'])
	if pard['user_data']['fullname']:
		pard['user_html'] = pard['user_data']['fullname']
	else:
		pard['user_html'] = pard['user_data']['email']
	
	pard['account_settings_url'] = gauth.get_account_setting_url()
	pard['user_menu'] = '''
		<li class="dropdown">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		     %(user_html)s
		     <span class="caret"></span>
		  </a>
		  <ul class="dropdown-menu">
			<li><a href="%(account_settings_url)s">Settings</a></li>
			<li role="separator" class="divider"></li>
			<li><a href="%(LOGOUT_URL)s">Logout</a></li>
		  </ul>
		</li>
		''' % pard
	
	pard['html'] = template.render('navbar_fixed_top.html', pard, force_reload=True)
	return pard



