import config
from wsgian import gauth
import sys
import wsgian

urls = [		
	{'pattern': '/',  'module': 'home'},
	{'pattern': '/pippo',  'module': 'home', 'action': 'pippo', 'login': True}
]

try:
	config = config.config({})
except ImportError:
	print 'config not found: run "python config.py" then edit site_config.py'
	sys.exit(1)

gauth.test_database(config)

urls.extend(gauth.auth_urls(config))

app = wsgian.App(urls, config)

if __name__ == '__main__':
	wsgian.quickstart(app)

