import pprint
import os

def config(pard):
	import site_config
	for k in ['LANGUAGES',
			 'APPSERVER',
			 'PROJECT_TITLE',
			 'TITLE',
			 'LOGLEVEL',
			 'PROJECT_SUBTITLE',
			 'PROJECT',
			 'APPLICATION_NAME',
			 'with_static',
			 'cookie_secret',
			 'LOGOUT_URL',
			 'db.mysql',
			 'DEFAULT_SENDER',
			 'DEBUG',
			 'LOGIN_URL',
			 'LOGIN_MODULE',
			 'SIGNUP_URL',
			 'AUTH_MODULE',
			 'GOOGLE',
			 ]:
		pard[k] = site_config.config.get(k)
	return pard


def build_default_config():
	if os.path.isfile('site_config.py'):
		pass
	else:
		print 'Creating default site configuration. . .',
		d = {
			'with_static': True,
			'DEBUG': True,
			'LOGLEVEL': 'debug',
			'cookie_secret': 'notapplicable',
			'APPLICATION_NAME': 'Application name',
			'TITLE': 'App Title',
			'PROJECT': 'Project key',
			'PROJECT_TITLE': 'Project title',
			'PROJECT_SUBTITLE': 'Project subtitle',
			'APPSERVER': 'Application Server URL (for example: http://localhost:8080)',
			'LANGUAGES': ['en'],
			
			# --------------------------------------------------- #
			# Autenticazione
			# --------------------------------------------------- #
			'AUTH_MODULE': 'gauth',
			'LOGIN_MODULE': 'gauth',
			'LOGIN_URL': '/login',
			'LOGOUT_URL': '/logout',
			'SIGNUP_URL': '/signup',
			
			# --------------------------------------------------- #
			# Email configuration
			# --------------------------------------------------- #
			'DEFAULT_SENDER': 'AppEmail@YourDomain.com',
			
			# --------------------------------------------------- #
			# Data Base
			# --------------------------------------------------- #
			'db.mysql': {
				'host': 'mysqlhost',
				'user': 'mysqluser',
				'password': 'mysqlpassword',
				'database': 'mysqldb',
				'max_idle_time': 7*3600}
			}
		buf = 'config = ' + pprint.pformat(d)
		open('site_config.py', 'w').write(buf)
		print 'done!'


if __name__ == '__main__':
	build_default_config()
